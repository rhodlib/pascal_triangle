﻿using System;

namespace NETCore
{
    class Program
    {
        static void Main(string[] args)
        {
            int floors = 0;
            int[] array = new int[1];
            /*Mostramos un mensaje y capturamos el dato ingresado
            desde la consola.*/
            Console.WriteLine("Ingrese el numero de pisos: ");

            floors = Convert.ToInt16(Console.ReadLine());
            
            /* Declaramos el primer ciclo for que va a recorrer dependiendo
            el dato ingresado que esta almacenado en la variable floors */

            for (int i = 0; i <= floors ; i++){
                /* Colocamos un arreglo y colocamos la variable i del ciclo for que sera
                la dimension que tendra el arreglo cada vez que el ciclo for se ejecute*/
                int[] pascal = new int[i];
                //Ciclo for que se decrementa para formar el triangulo.
                for (int j = floors; j < i; j--){
                    Console.Write(" ");
                }
                //Ciclo for que genera la sumas de las cifras.
                for (int k = 0; k < i; k++){
                    //Condicion que evalua la variable del ciclo for.
                    if(k == 0 || k == (i -1)){
                        pascal[k] = 1;
                    }else {
                        /*Sumamos los numeros que estan en cada posicion del arreglo
                        par formar el triangulo*/ 
                        pascal[k] = array[k] + array[k -1];
                    }
                    Console.Write("[" + pascal[k] + "]");
                }
                array = pascal;
                Console.WriteLine(" ");
            }
            Console.ReadLine();
        }
    }
}
